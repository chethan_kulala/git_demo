package com.example.gitdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

class MainActivity : AppCompatActivity() {

    private val fragmentManager: FragmentManager = supportFragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var fragment: Fragment
    private lateinit var fragmentAButton: Button
    private lateinit var fragmentBButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentAButton = findViewById(R.id.fragment_A)
        fragmentBButton = findViewById(R.id.fragment_B)

        fragment = SplashFragment()
        addFragment(fragment)

        fragmentAButton.setOnClickListener {
            Log.d("hello", "clicked")
            fragment = AFragment()
            addFragment(fragment)
        }

        fragmentBButton.setOnClickListener {
            fragment = BFragment()
            addFragment(fragment)
        }

    }

    private fun addFragment(fragment: Fragment) {
        fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.main_fragment, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        val count = fragmentManager.backStackEntryCount
        if (count != 0) {
            fragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

}